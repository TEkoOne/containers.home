'use strict';

angular.module('lxdAdmin.auth', ['ngRoute'])
    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/signup', {
            title: 'SignUp',
            templateUrl: 'modules/auth/signup.html',
            controller: 'signupCtrl'
        });
    }])

    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/signin', {
            title: 'SignIn',
            templateUrl: 'modules/auth/signin.html',
            controller: 'signinCtrl'
        });
    }])

    .controller('signupCtrl', function($scope, $http, $timeout, $interval, authService) {

      $scope.savedSuccessfully = false;
      $scope.message = '';
      $scope.registration = {
        login:'',
        password:'',
        confirm:''
      }

      $scope.signUp = function() {
        authService.signUp($scope.registration).then(function(data) {
          console.log(data);
          $scope.message = data.data.container_id;
          $scope.savedSuccessfully = true;
          // console.log(data);
      //     var refreshInterval;
      //     refreshInterval = $interval(
      //       function() {
      //         authService.isReady(data.data).then(function(data) {
      //         if (data.data.status) {
      //           $interval.cancel(refreshInterval);
      //           authService.signIn($scope.registration).then(function(data) {
      //             $scope.message = data.data.container_id;
      //             if (authService.isAuth()) {
      //               window.location = "#/containers";
      //             }
      //           })
      //         };
      //       });
      //  },2000);
     })
    }
  })

    .controller('signinCtrl', function($scope, $http, $interval, authService) {

      if (authService.isAuth()) {
        window.location = "#/containers";
      }

      $scope.message = '';
      $scope.login = {
        login:'',
        password:''
      }

      $scope.signIn = function() {
        authService.signIn($scope.login).then(function(data) {
          // console.log(data);
          $scope.message = data.data.status;
          if (authService.isAuth()) {
            window.location = "#/containers";
          }
        })
      };

    })

    .controller('indexCtrl', function($scope, $http, $interval, authService) {

      $scope.Auth = ''

      $scope.isAuth = function() {
        return authService.isAuth();
      };

      $scope.fillAuth = function() {
        $scope.Auth = authService.fillAuth();
        console.log($scope.Auth);
      };

      $scope.logOut = function() {
        return authService.signOut();
      };

      $scope.fillAuth();

    })

  ;
