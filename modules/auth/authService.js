'use strict';

angular.module('lxdAdmin.auth')
    .factory('authService', ['$http', '$q','settings','localStorageService',
        function ($http, $q, settings,localStorageService) {
            var obj = {};

            obj.signUp = function (registerData) {
                var deferred = $q.defer();
                $http.post(settings.apiUrl + '/signup', registerData).then(function(data) {
                  deferred.resolve(data);
                });
                return deferred.promise;
            };

            obj.signIn = function (loginData) {
                var deferred = $q.defer();
                $http.post(settings.apiUrl + '/signin', loginData).then(function(data) {
                  if (data.data.container_id) {
                    localStorageService.set('authorizationData', {container_id: data.data.container_id, login: loginData.login});
                    window.location = "#/signin";
                  }
                  deferred.resolve(data);
                });
                return deferred.promise;
            };

            obj.signOut = function () {
                localStorageService.remove('authorizationData');
                window.location = "#/signin";
            };

            obj.isAuth = function () {
                var authData = localStorageService.get('authorizationData');
                if (authData) {
                    return true;
                }
            };

            obj.fillAuth = function () {
                var authData = localStorageService.get('authorizationData');
                return authData;
            };

            obj.isReady  = function (containerData) {
                return $http.post(settings.apiUrl + '/status', containerData)
            };

            obj.getContainer = function () {
                var authData = localStorageService.get('authorizationData');
                if (authData) {
                    return '/id/' + authData.container_id;
                }
                else {
                    window.location = "#/signin";
                }
            };

            return obj;
        }])
;
