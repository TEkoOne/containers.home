'use strict';

angular.module('lxdAdmin.images', ['ngRoute'])
    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/images', {
            title: 'Images',
            templateUrl: 'modules/images/images.html',
            controller: 'imagesListCtrl',
            resolve: {
                images: function (imagesService) {
                    return imagesService.getAll();
                }
            }
        })

        ;
    }])

    .controller('imagesListCtrl', function($scope, $http, $interval, imagesService, images) {

    $scope.images = images;

  });
