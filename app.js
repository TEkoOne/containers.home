'use strict';

angular.module('lxdAdmin', [
  'ngRoute',
  'ui.bootstrap',
  // 'ui.select',
  'angular-loading-bar',
  'LocalStorageModule',
  'lxdAdmin.containers',
  'lxdAdmin.images',
  'lxdAdmin.terminal',
  'lxdAdmin.auth'
]).

config(['$routeProvider', function($routeProvider) {
  $routeProvider.otherwise({redirectTo: '/containers'});
}]).

constant('settings', {
  apiUrl: '/api',
  version: '/1.0'
});
