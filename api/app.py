from flask import Flask
from flask import Response
from flask import request
from flask import g
from io import BytesIO

import pycurl
# import subprocess
# import os
import json
import random
import string
import sqlite3
import bcrypt

WORKDIR = '/var/www/containers.home/'
DATABASE = WORKDIR + 'api/app.db'
LXD_HOST_API = 'http://lxd.home.tekoone.ru/api/1.0/'
LXD_TEMPLATE = 'nested-template'

# from git import Repo
app = Flask(__name__)

def create_container(name):
    buffer = BytesIO()
    c = pycurl.Curl()
    c.setopt(c.URL, LXD_HOST_API + 'containers')
    c.setopt(c.HTTPHEADER, ['Content-Type: application/json'])
    data = {'name':name,'profiles':['default'],'ephemeral':False,\
    'source':{'type':'image','alias':LXD_TEMPLATE}}
    c.setopt(c.POSTFIELDS, json.dumps(data))
    c.setopt(c.WRITEDATA, buffer)
    c.perform()
    c.close()
    body = buffer.getvalue()
    return True

def start_container(name):
    buffer = BytesIO()
    c = pycurl.Curl()
    c.setopt(c.URL, LXD_HOST_API + 'containers/' + name + '/state')
    c.setopt(c.HTTPHEADER, ['Content-Type: application/json'])
    data = {'action':'start','timeout':30,'force':True,'stateful':False}
    c.setopt(c.CUSTOMREQUEST, "PUT")
    c.setopt(c.POSTFIELDS, json.dumps(data))
    c.setopt(c.WRITEDATA, buffer)
    c.perform()
    c.close()
    body = buffer.getvalue()
    return True

def status_container(data):
    buffer = BytesIO()
    c = pycurl.Curl()
    c.setopt(c.URL, LXD_HOST_API + 'containers/' + data['container_id'] + '/state')
    c.setopt(c.WRITEDATA, buffer)
    c.perform()
    c.close()
    body = buffer.getvalue()
    status = json.loads(body.decode())
    # return status['metadata']['network']
    if status['metadata']['network']:
        if status['metadata']['network']['eth0']:
            if status['metadata']['network']['eth0']['addresses']:
                for a in status['metadata']['network']['eth0']['addresses']:
                    if (a['address'].startswith('192.168')):
                        return 'ready'

def init_db():
    with app.app_context():
        db = get_db()
        with app.open_resource(WORKDIR + 'api/app.db.sql', mode='r') as f:
            db.cursor().executescript(f.read())
        db.commit()

def get_db():
    db = getattr(g, '_database', None)
    if db is None:
        db = g._database = sqlite3.connect(DATABASE)
    return db

@app.teardown_appcontext
def close_connection(exception):
    db = getattr(g, '_database', None)
    if db is not None:
        db.close()

def db_query(query, args=(), one=False):
    cur = get_db().execute(query, args)
    rv = cur.fetchall()
    cur.close()
    return (rv[0] if rv else None) if one else rv

def db_insert(data):
    host = gen_host()
    if create_container(host):
        hashed = bcrypt.hashpw(data['password'].encode('utf-8'), bcrypt.gensalt())
        get_db().execute('insert into users(login,password,container_id) \
        values (?,?,?)', [data['login'], hashed, host])
        get_db().commit()
        return host
    else:
        return False

def gen_host():
    s = string.ascii_lowercase + string.digits
    base =  ''.join(random.sample(string.ascii_lowercase,3))
    hostname = ''.join(random.sample(s,13))
    hostname = base + hostname
    return hostname

def send(data, name = 'status'):
    d = dict()
    d[name] = data
    resp = Response(json.dumps(d))
    resp.headers['Content-Type'] = 'application/json'
    return resp

def cookie_insertion(name,value):
    d = dict()
    d[name] = value
    resp = Response(json.dumps(d))
    resp.headers['Content-Type'] = 'application/json'
    resp.set_cookie(name,value=value)
    return resp

@app.route("/ping")
def ping():
	return send('pong')

@app.route("/signup", methods=['POST'])
def signup():
    # try:
    data = request.get_json(silent=True)
    # print(data['login'], flush=True)
    if not (data['login']):
        return send('Wrong login')
    if not (data['password']):
        return send('Wrong password')
    if db_query('select login from users where login=?',[data['login']]):
        return send('Already registred')
    if (data['password'] == data['confirm']):
        host = db_insert(data)
        if host:
            # start_container(host)
            return send(host,'container_id')
        else:
            return send('Failed')
    else:
        return send('Passwords missmatch')
    # except KeyError:
    #     return send('Error in data')

@app.route("/signin", methods=['POST'])
def signin():
    try:
        data = request.get_json(silent=True)
        if not (data['login']):
            return send('Wrong login')
        if not (data['password']):
            return send('Wrong password')
        hashed = db_query('select password from users where login=?',[data['login']],True)
        if hashed:
            if len(hashed)>0:
                hashed = hashed[0]
                if bcrypt.hashpw(data['password'].encode('utf-8'), hashed) == hashed:
                    hostname = db_query('select container_id from users where login=?',[data['login']],True)
                    if hostname:
                        # start_container(hostname[0])
                        return send(hostname[0],'container_id')
                    else:
                        return send('Wrong hostname')
                else:
                    return send('Wrong password')
        else:
            return send('Wrong credentials')
    except KeyError:
        return send('Error in data')

@app.route("/init", methods=['GET'])
def init():
    try:
        init_db()
        return send('DB Init - Success')
    except sqlite3.OperationalError:
        return send('DB Init - Error')

@app.route("/status", methods=['POST'])
def status():
    data = request.get_json(silent=True)
    if data:
        return send(status_container(data))
    else:
        return send('')


if __name__ == "__main__":
    app.run(host='0.0.0.0', port=5060)
