BEGIN TRANSACTION;
CREATE TABLE `users` (
	`id`	INTEGER PRIMARY KEY AUTOINCREMENT,
	`login`	TEXT UNIQUE,
	`password`	TEXT,
	`container_id`	TEXT UNIQUE
);
COMMIT;
